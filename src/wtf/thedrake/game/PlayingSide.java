package wtf.thedrake.game;

/**
 * Representation of playing side.
 */
public enum PlayingSide {
    BLUE {
        @Override
        public PlayingSide opposite() {
            return ORANGE;
        }
    },
    ORANGE;

    public PlayingSide opposite() {
        return BLUE;
    }
}
