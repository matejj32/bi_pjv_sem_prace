package wtf.thedrake.game;

import java.util.ArrayList;
import java.util.List;

/**
 * Representation of troop info.
 */
public class TroopInfo {
    /**
     * Name of this troop.
     */
    private final String name;

    /**
     * 2D offset of front pivot.
     */
    private final Offset2D frontPivot;

    /**
     * 2D offset of back pivot.
     */
    private final Offset2D backPivot;

    private final List<TroopAction> frontActions;
    private final List<TroopAction> backActions;

    /**
     * Constructor sets name, front and back pivot.
     *
     * @param name       name of this troop
     * @param frontPivot 2D offset of front side
     * @param backPivot  2D offset of back side
     */
    public TroopInfo(String name, Offset2D frontPivot, Offset2D backPivot, List<TroopAction> frontActions, List<TroopAction> backActions) {
        this.name = name;
        this.frontPivot = frontPivot;
        this.backPivot = backPivot;
        this.frontActions = frontActions;
        this.backActions = backActions;
    }

    public TroopInfo(TroopInfo old) {
        this.name = old.name;
        this.frontPivot = old.frontPivot;
        this.backPivot = old.backPivot;

        this.frontActions = new ArrayList<>();
        this.frontActions.addAll(old.frontActions);

        this.backActions = new ArrayList<>();
        this.backActions.addAll(old.backActions);
    }

    /**
     * Constructor sets name and same 2D offset for front and back pivot.
     *
     * @param name  name of this troop
     * @param pivot 2D offset of both sides
     */
    public TroopInfo(String name, Offset2D pivot, List<TroopAction> frontActions, List<TroopAction> backActions) {
        this(name, pivot, pivot, frontActions, backActions);
    }

    /**
     * Constructor sets name and sets default 2D offset for both sides
     *
     * @param name name of this troop
     */
    public TroopInfo(String name, List<TroopAction> frontActions, List<TroopAction> backActions) {
        this(name, new Offset2D(1, 1), frontActions, backActions);
    }

    public List<TroopAction> actions(TroopFace face) {
        if (face == TroopFace.FRONT) {
            return frontActions;
        }

        return backActions;
    }

    /**
     * Return a name of this troop.
     *
     * @return String
     */
    public String name() {
        return name;
    }

    /**
     * Returns 2D offset of this troop determined by a given face.
     *
     * @param face face of troop
     * @return Offset2D
     */
    public Offset2D pivot(TroopFace face) {
        if (face == TroopFace.FRONT)
            return frontPivot;

        return backPivot;
    }
}
