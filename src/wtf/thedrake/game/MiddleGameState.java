package wtf.thedrake.game;

import wtf.thedrake.media.GameStateMedia;

import java.util.ArrayList;
import java.util.List;

public class MiddleGameState extends BaseGameState {

    public MiddleGameState(
            Board board,
            TroopStacks troopStacks,
            BothLeadersPlaced leaders,
            PlayingSide sideOnTurn) {
        super(
                board,
                troopStacks,
                leaders,
                sideOnTurn);
    }

    @Override
    public BothLeadersPlaced leaders() {
        return (BothLeadersPlaced) super.leaders();
    }

    /* Všechny tahy, které může hráč, jenž je zrovna a tahu, provést
     * v tomto stavu hry.
     */
    @Override
    public List<Move> allMoves() {
        List<Move> result = new ArrayList<>();

        for (Tile tile : board()) {
            result.addAll(boardMoves(tile.position()));
        }

        result.addAll(stackMoves());

        return result;
    }

    /* Všechny tahy, které může hráč, jenž je zrovna a tahu, provést
     * z políčka na pozici position.
     */
    @Override
    public List<Move> boardMoves(TilePosition position) {
        List<Move> result = new ArrayList<>();

        if (!board().tileAt(position).hasTroop() || board().tileAt(position).troop().side() != sideOnTurn()) {
            return result;
        }

        Troop troop = board().tileAt(position).troop();

        for (BoardChange boardChange : troop.changesFrom(position, board())) {
            result.add(new BoardMove(this, boardChange));
        }

        return result;
    }

    /* Všechny tahy, které může hráč, jenž je zrovna a tahu, provést
     * ze zásobníku.
     */
    @Override
    public List<Move> stackMoves() {
        List<Move> result = new ArrayList<>();

        for (Tile tile : board()) {
            if (canPlaceFromStack(tile.position())) {
                result.add(new PlaceFromStack(this, tile.position()));
            }
        }

        return result;
    }

    /* Už někdo vyhrál? Pokud ano, vítězem je hráč, který právě NENÍ tahu.
     * Nebo-li prohrává ten, kdo je právě na tahu, ale už nemůže táhnout.
     */
    @Override
    public boolean isVictory() {
        return false;
    }

    public boolean canPlaceFromStack(TilePosition position) {
        if (!board().contains(position) || board().tileAt(position).hasTroop())
            return false;

        boolean hasSideTroopAsNeigbour = false;
        hasSideTroopAsNeigbour |= isNeighbourSideTroop(position, 0, 1);
        hasSideTroopAsNeigbour |= isNeighbourSideTroop(position, 0, -1);
        hasSideTroopAsNeigbour |= isNeighbourSideTroop(position, 1, 0);
        hasSideTroopAsNeigbour |= isNeighbourSideTroop(position, -1, 0);

        return hasSideTroopAsNeigbour;
    }

    private boolean isNeighbourSideTroop(TilePosition origin, int xStep, int yStep) {
        return board().contains(origin.step(xStep, yStep))
                && board().tileAt(origin.step(xStep, yStep)).hasTroop()
                && board().tileAt(origin.step(xStep, yStep)).troop().side() == sideOnTurn();
    }

    public MiddleGameState placeFromStack(TilePosition target) {
        Troop troop = troopStacks().peek(sideOnTurn());
        return new MiddleGameState(
                board().withTiles(
                        new TroopTile(target, troop)),
                troopStacks().pop(sideOnTurn()),
                leaders(),
                sideOnTurn().opposite());
    }

    @Override
    public <T> T putToMedia(GameStateMedia<T> media) {
        return media.putMiddleGameState(this);
    }
}
