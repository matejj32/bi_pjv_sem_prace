package wtf.thedrake.game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CapturedTroops {

    public ArrayList<TroopInfo> getCapturedBlueTroops() {
        return capturedBlueTroops;
    }

    public ArrayList<TroopInfo> getCapturedOrangeTroops() {
        return capturedOrangeTroops;
    }

    private final ArrayList<TroopInfo> capturedBlueTroops;
    private final ArrayList<TroopInfo> capturedOrangeTroops;

    /**
     * Constructor creates empty lists.
     */
    public CapturedTroops() {
        capturedBlueTroops = new ArrayList<>();
        capturedOrangeTroops = new ArrayList<>();
    }

    /**
     * Constructor creates list by given parameters with two lists.
     * @param blueTroops
     * @param orangeTroops
     */
    public CapturedTroops(List<TroopInfo> blueTroops, List<TroopInfo> orangeTroops){
        capturedBlueTroops = (ArrayList<TroopInfo>) blueTroops;
        capturedOrangeTroops = (ArrayList<TroopInfo>) orangeTroops;
    }

    /**
     * Constructor creates list by given parameters.
     * @param old
     */
    public CapturedTroops(CapturedTroops old) {
        this.capturedBlueTroops = new ArrayList<>();
        this.capturedBlueTroops.addAll(old.capturedBlueTroops);

        this.capturedOrangeTroops = new ArrayList<>();
        this.capturedOrangeTroops.addAll(old.capturedOrangeTroops);
    }

    /**
     * Returns a list of captured troops based on given side.
     *
     * @param side playing side
     * @return list of troop info
     */
    public List<TroopInfo> troops(PlayingSide side) {
        if (side == PlayingSide.BLUE) {
            return Collections.unmodifiableList(capturedBlueTroops);
        }

        return Collections.unmodifiableList(capturedOrangeTroops);
    }

    /**
     * Adds newly captured troop at the beginning of captured troops list.
     *
     * @param side playing side
     * @param info information about troop
     * @return this
     */
    public CapturedTroops withTroop(PlayingSide side, TroopInfo info) {
        CapturedTroops tmp = new CapturedTroops(this);

        if (side == PlayingSide.BLUE) {
            tmp.capturedBlueTroops.add(0, info);
        } else {
            tmp.capturedOrangeTroops.add(0, info);
        }

        return tmp;
    }
}
