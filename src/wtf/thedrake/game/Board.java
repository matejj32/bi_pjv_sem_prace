package wtf.thedrake.game;

import wtf.thedrake.media.BoardMedia;

import java.util.Iterator;

/**
 * Representation of board.
 */
public class Board implements Iterable<Tile> {
    /**
     * Dimension of this board.
     */
    private final int dimension;

    /**
     * Captured troops.
     */
    private CapturedTroops capturedTroops;

    /**
     * 2D array of tiles of this board.
     */
    private Tile tiles[][];

    /**
     * Constructor creates a 2D board with given tiles. Other tiles are empty.
     *
     * @param dimension dimension of this board
     * @param tiles     list of tiles of this board
     */
    public Board(int dimension, Tile... tiles) {
        capturedTroops = new CapturedTroops();

        this.dimension = dimension;
        this.tiles = new Tile[dimension][dimension];
        Tile tmp_tile;

        for (int i = 0; i < dimension; i++) {
            for (int j = 0; j < dimension; j++) {
                tmp_tile = new EmptyTile(new TilePosition(i, j));
                this.tiles[i][j] = tmp_tile;
            }
        }

        for (Tile tile : tiles) {
            this.tiles[tile.position().i][tile.position().j] = tile;
        }
    }

    /**
     * Primary constructor
     *
     * @param dimension
     * @param captured
     * @param tiles
     */
    public Board(int dimension, CapturedTroops captured, Tile... tiles) {
        this(dimension, tiles);
        this.capturedTroops = captured;
    }

    /**
     * Copy constructor
     *
     * @param old
     */
    private Board(Board old) {
        this.dimension = old.dimension;
        this.tiles = new Tile[dimension][];

        for (int i = 0; i < dimension; i++) {
            this.tiles[i] = old.tiles[i].clone();
        }

        this.capturedTroops = new CapturedTroops(old.capturedTroops);
    }

    /**
     * Returns dimension of this board.
     *
     * @return int
     */
    public int dimension() {
        return dimension;
    }

    /**
     * Returns a tile of the given position. If the position is not inside this board, IllegalArgumentException is thrown.
     *
     * @param position position of tile to return
     * @return Tile
     */
    public Tile tileAt(TilePosition position) throws IllegalArgumentException {
        if (contains(position))
            return tiles[position.i][position.j];
        else
            throw new IllegalArgumentException("Tile position doesn't exist.");
    }

    /**
     * Determines if each of positions is inside this board.
     *
     * @param positions list of positions
     * @return boolean
     */
    public boolean contains(TilePosition... positions) {
        for (TilePosition position : positions) {
            if (position.i < 0 || position.i >= dimension || position.j < 0 || position.j >= dimension)
                return false;
        }
        return true;
    }

    /**
     * Creates a new board with new tiles, all other tiles remains the same.
     *
     * @param tiles new tiles to add
     * @return Board
     */
    public Board withTiles(Tile... tiles) {
        Board tmp = new Board(this);

        for (Tile tile : tiles) {
            tmp.tiles[tile.position().i][tile.position().j] = tile;
        }

        return tmp;
    }

    public Board withCaptureAndTiles(TroopInfo info, PlayingSide side, Tile... tiles) {
        Board tmp = withTiles(tiles);
        tmp.capturedTroops = capturedTroops.withTroop(side, info);
        return tmp;
    }

    // Vrací zajaté jednotky
    public CapturedTroops captured() {
        return capturedTroops;
    }

    // Stojí na pozici origin jednotka?
    public boolean canTakeFrom(TilePosition origin) {
        return contains(origin) && tileAt(origin).hasTroop();
    }

    /*
     * Lze na danou pozici postavit zadanou jednotku? Zde se řeší pouze
     * jednotky na hrací ploše, nikoliv zásobník, takže se v podstatě
     * pouze ptám, zda dlaždice na pozici target přijme danou jednotku.
     */
    public boolean canPlaceTo(Troop troop, TilePosition target) {
        return contains(target) && tileAt(target).acceptsTroop(troop);
    }

    // Může zadaná jednotka zajmout na pozici target soupeřovu jednotku?
    public boolean canCaptureOn(Troop troop, TilePosition target) {
        return contains(target) && tileAt(target).hasTroop() && tileAt(target).troop().side() != troop.side();
    }

    /*
     * Stojí na políčku origin jednotka, která může udělat krok na pozici target
     * bez toho, aby tam zajala soupeřovu jednotku?
     */
    public boolean canStepOnly(TilePosition origin, TilePosition target) {
        return contains(origin, target) && tileAt(origin).hasTroop() && !tileAt(target).hasTroop();
    }

    /*
     * Stojí na políčku origin jednotka, která může zůstat na pozici origin
     * a zajmout soupeřovu jednotku na pozici target?
     */
    public boolean canCaptureOnly(TilePosition origin, TilePosition target) {
        return contains(origin, target)
                && tileAt(origin).hasTroop()
                && tileAt(target).hasTroop()
                && tileAt(origin).troop().side() != tileAt(target).troop().side();
    }

    /*
     * Stojí na pozici origin jednotka, která může udělat krok na pozici target
     * a zajmout tam soupeřovu jednotku?
     */
    public boolean canStepAndCapture(TilePosition origin, TilePosition target) {
        return contains(origin, target)
                && tileAt(origin).hasTroop()
                && tileAt(target).hasTroop()
                && tileAt(origin).troop().side() != tileAt(target).troop().side();
    }

    /*
     * Nová hrací deska, ve které jednotka na pozici origin se přesunula
     * na pozici target bez toho, aby zajala soupeřovu jednotku.
     */
    public Board stepOnly(TilePosition origin, TilePosition target) {
        Troop attacker = tileAt(origin).troop();

        return withTiles(
                new EmptyTile(origin),
                new TroopTile(target, attacker.flipped()));
    }

    /*
     * Nová hrací deska, ve které jednotka na pozici origin se přesunula
     * na pozici target, kde zajala soupeřovu jednotku.
     */
    public Board stepAndCapture(TilePosition origin, TilePosition target) {
        Troop attacker = tileAt(origin).troop();
        Troop targetTroop = tileAt(target).troop();

        return withCaptureAndTiles(
                targetTroop.info(),
                targetTroop.side(),
                new EmptyTile(origin),
                new TroopTile(target, attacker.flipped()));
    }

    /*
     * Nová hrací deska, ve které jednotka zůstává stát na pozici origin
     * a zajme soupeřovu jednotku na pozici target.
     */
    public Board captureOnly(TilePosition origin, TilePosition target) {
        Troop attacker = tileAt(origin).troop();
        Troop targetTroop = tileAt(target).troop();

        return withCaptureAndTiles(
                targetTroop.info(),
                targetTroop.side(),
                new EmptyTile(target),
                new TroopTile(origin, attacker.flipped()));
    }

    /**
     * Method returns new instance of BoardIterator.
     *
     * @return iterator
     */
    @Override
    public Iterator<Tile> iterator() {
        return new BoardIterator();
    }

    /**
     * This class represents own iterator for iterating via for-each cycle and implements Iterator interface.
     */
    private class BoardIterator implements Iterator<Tile> {
        private int i;
        private int j;
        private boolean last = false;

        public BoardIterator() {
            i = 0;
            j = 0;
        }

        @Override
        public boolean hasNext() {
            if (last)
                return false;
            if (i + 1 == dimension) {
                if (j + 1 == dimension) {
                    last = true;
                }
            }
            return true;
        }

        @Override
        public Tile next() {
            int temp_i = i;
            int temp_j = j;
            i++;
            if (i == dimension) {
                j++;
                i = 0;
                if (j == dimension) {
                    j = 0;
                }
            }

            return tiles[temp_i][temp_j];
        }
    }

    public <T> T putToMedia(BoardMedia<T> media) {
        return media.putBoard(this);
    }
}
