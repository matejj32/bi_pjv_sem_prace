package wtf.thedrake.game;

import java.util.ArrayList;
import java.util.List;

/**
 * Representation of a troop in a game.
 */
public class Troop {
    /**
     * Information about this troop.
     */
    private final TroopInfo info;

    /**
     * Playing side of this troop.
     */
    private final PlayingSide side;

    /**
     * Current face of this troop.
     */
    private final TroopFace face;

    /**
     * Constructor sets info, side and face.
     *
     * @param info informations
     * @param side side
     * @param face face
     */
    public Troop(TroopInfo info, PlayingSide side, TroopFace face) {
        this.info = info;
        this.side = side;
        this.face = face;
    }

    /**
     * Constructor sets info, side and default face to front side.
     *
     * @param info
     * @param side
     */
    public Troop(TroopInfo info, PlayingSide side) {
        this(info, side, TroopFace.FRONT);
    }

    /**
     * Returns information about this troop.
     *
     * @return TroopInfo
     */
    public TroopInfo info() {
        return info;
    }

    /**
     * Returns side of this troop.
     *
     * @return PlayingSide
     */
    public PlayingSide side() {
        return side;
    }

    /**
     * Returns current face of this troop.
     *
     * @return TroopFace
     */
    public TroopFace face() {
        return face;
    }

    /**
     * Returns 2D offset of troop's pivot.
     *
     * @return Offset2D
     */
    public Offset2D pivot() {
        return info.pivot(face);
    }

    /**
     * Returns flipped troop to other side.
     *
     * @return Troop
     */
    public Troop flipped() {
        return new Troop(info, side, (face == TroopFace.FRONT ? TroopFace.BACK : TroopFace.FRONT));
    }

    /**
     * Všechny změny desky, které může jednotka provést na desce board, pokud stojí na pozici pos.
     *
     * @param pos
     * @param board
     * @return List<BoardChange>
     */
    public List<BoardChange> changesFrom(TilePosition pos, Board board) {
        List<BoardChange> boardChangeList = new ArrayList<>();

        if (board.contains(pos)) {
            for (TroopAction troopAction : info().actions(face())) {
                boardChangeList.addAll(troopAction.changesFrom(pos, side(), board));
            }
        }

        return boardChangeList;
    }
}
