package wtf.thedrake.game;

import wtf.thedrake.media.TileMedia;

/**
 * Representation of troop tile.
 */
public class TroopTile extends Tile {
    /**
     * Troop in this tile.
     */
    private Troop troop;

    /**
     * @param position position of tile
     * @param troop    troop
     */
    public TroopTile(TilePosition position, Troop troop) {
        super(position);
        this.troop = troop;
    }

    @Override
    public boolean hasTroop() {
        return true;
    }

    @Override
    public Troop troop() {
        return troop;
    }

    @Override
    public boolean acceptsTroop(Troop troop) {
        return false;
    }

    public <T> T putToMedia(TileMedia<T> media) {
        return media.putTroopTile(this);
    }
}
