package wtf.thedrake.game;

/**
 * Representation of offset in 2D space.
 */
public class Offset2D {
    /**
     * X-axis value.
     */
    public final int x;

    /**
     * Y-axis value.
     */
    public final int y;

    /**
     * Constructor sets x and y axis values.
     *
     * @param x x-axis coordination
     * @param y y-axis coordination
     */
    public Offset2D(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Determines if this offset equals to given offset.
     *
     * @param x x-axis coordination
     * @param y y-axis coordination
     * @return boolean
     */
    public boolean equalsTo(int x, int y) {
        return (this.x == x && this.y == y);
    }

    /**
     * Returns new flipped offset with flipped y-axis coordination.
     *
     * @return Offset2D
     */
    public Offset2D yFlipped() {
        return new Offset2D(this.x, -this.y);
    }
}
