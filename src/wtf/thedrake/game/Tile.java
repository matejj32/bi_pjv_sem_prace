package wtf.thedrake.game;


import wtf.thedrake.media.TileMedia;

/**
 * Tile representation.
 */
public abstract class Tile {
    /**
     * Position of this tile.
     */
    private TilePosition position;

    /**
     * A protected constructor which creates a tile.
     *
     * @param position position of this tile
     */
    protected Tile(TilePosition position) {
        this.position = position;
    }

    /**
     * Return a position of this tile inside the board.
     *
     * @return TilePosition
     */
    public TilePosition position() {
        return position;
    }

    /**
     * Determines if a troop can be placed on top of this tile.
     *
     * @param troop troop to test
     * @return boolean
     */
    public abstract boolean acceptsTroop(Troop troop);

    /**
     * Determines if this tile has a troop on top of itself.
     *
     * @return boolean
     */
    public abstract boolean hasTroop();

    /**
     * Returns a current troop on top of this tile. If this tile is empty, UnsupportedOperationException is thrown.
     *
     * @return Troop
     */
    public abstract Troop troop() throws UnsupportedOperationException;

    public abstract <T> T putToMedia(TileMedia<T> media);
}
