package wtf.thedrake.game;

import wtf.thedrake.media.TileMedia;

/**
 * Empty tile representation.
 */
public class EmptyTile extends Tile {
    /**
     * Constructor creates Empty tile through the super constructor.
     *
     * @param position position of tile
     */
    public EmptyTile(TilePosition position) {
        super(position);
    }

    @Override
    public boolean hasTroop() {
        return false;
    }

    @Override
    public Troop troop() throws UnsupportedOperationException {
        throw new UnsupportedOperationException("Empty tile doesn't contain Troop.");
    }

    @Override
    public boolean acceptsTroop(Troop troop) {
        return true;
    }

    public <T> T putToMedia(TileMedia<T> media) {
        return media.putEmptyTile(this);
    }
}
