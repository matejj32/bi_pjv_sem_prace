package wtf.thedrake.game;

/**
 * Representation of front vs. back side of a troop.
 */
public enum TroopFace {
    FRONT,
    BACK
}
