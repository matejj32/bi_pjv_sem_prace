package wtf.thedrake.media;

import wtf.thedrake.game.EmptyTile;
import wtf.thedrake.game.TroopTile;

public interface TileMedia<T> {
	public T putTroopTile(TroopTile tile);	
	public T putEmptyTile(EmptyTile tile);
}
