package wtf.thedrake.media;

import wtf.thedrake.game.CapturedTroops;

public interface CapturedTroopsMedia<T> {
	public T putCapturedTroops(CapturedTroops captured);
}
