package wtf.thedrake.media;

import wtf.thedrake.game.BothLeadersPlaced;
import wtf.thedrake.game.NoLeadersPlaced;
import wtf.thedrake.game.OneLeaderPlaced;

public interface LeadersMedia<T> {
	public T putNoLeadersPlaced(NoLeadersPlaced leaders);
	public T putOneLeaderPlaced(OneLeaderPlaced leaders);
	public T putBothLeadersPlaced(BothLeadersPlaced leaders);
}
