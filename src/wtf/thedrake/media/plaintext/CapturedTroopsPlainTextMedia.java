package wtf.thedrake.media.plaintext;

import wtf.thedrake.game.*;
import wtf.thedrake.media.CapturedTroopsMedia;
import wtf.thedrake.media.PrintMedia;

import java.io.OutputStream;
import java.io.PrintWriter;

public class CapturedTroopsPlainTextMedia extends PrintMedia implements CapturedTroopsMedia<Void> {
    public CapturedTroopsPlainTextMedia(OutputStream stream) {
        super(stream);
    }

    @Override
    public Void putCapturedTroops(CapturedTroops captured) {
        PrintWriter w = writer();

        w.println("Captured BLUE: " + captured.troops(PlayingSide.BLUE).size());
        for(TroopInfo info : captured.troops(PlayingSide.BLUE)){
            w.println(info.name());
        }

        if(captured.troops(PlayingSide.ORANGE).isEmpty()){
            w.printf("%s", "Captured ORANGE: " + captured.troops(PlayingSide.ORANGE).size());
        }else{
            w.println("Captured ORANGE: " + captured.troops(PlayingSide.ORANGE).size());
            for(TroopInfo info : captured.troops(PlayingSide.ORANGE)){
                if(captured.troops(PlayingSide.ORANGE).indexOf(info) == captured.troops(PlayingSide.ORANGE).size()-1){
                    w.printf("%s", info.name());
                }else {
                    w.println(info.name());
                }
            }
        }

        return null;
    }
}
