package wtf.thedrake.media.plaintext;

import wtf.thedrake.game.EmptyTile;
import wtf.thedrake.game.PlayingSide;
import wtf.thedrake.game.TroopFace;
import wtf.thedrake.game.TroopTile;
import wtf.thedrake.media.PrintMedia;
import wtf.thedrake.media.TileMedia;

import java.io.OutputStream;
import java.io.PrintWriter;

public class TilePlainTextMedia extends PrintMedia implements TileMedia<Void> {
    public TilePlainTextMedia(OutputStream stream) {
        super(stream);
    }

    @Override
    public Void putTroopTile(TroopTile tile) {
        PrintWriter w = writer();

        w.println(tile.troop().info().name()
                + (tile.troop().side() == PlayingSide.BLUE ? " BLUE " : " ORANGE ")
                + (tile.troop().face() == TroopFace.FRONT ? "FRONT" : "BACK"));

        return null;
    }

    @Override
    public Void putEmptyTile(EmptyTile tile) {
        PrintWriter w = writer();

        w.println("empty");

        return null;
    }
}
