package wtf.thedrake.media.plaintext;

import wtf.thedrake.game.*;
import wtf.thedrake.media.GameStateMedia;
import wtf.thedrake.media.PrintMedia;

import java.io.OutputStream;
import java.io.PrintWriter;

public class GameStatePlainTextMedia extends PrintMedia implements GameStateMedia<Void> {
    private final BoardPlainTextMedia boardPlainTextMediamedia;
    private final TroopStacksPlainTextMedia troopStacksPlainTextMedia;

    public GameStatePlainTextMedia(OutputStream stream) {
        super(stream);
        boardPlainTextMediamedia = new BoardPlainTextMedia(stream);
        troopStacksPlainTextMedia = new TroopStacksPlainTextMedia(stream);
    }

    @Override
    public Void putPlacingLeadersGameState(PlacingLeadersGameState state) {
        PrintWriter w = writer();

        w.println("LEADERS");
        w.println(0); // pocet strazi - pri staveni vudcu v prvni fazi nemuze nastat, ze by byli na plose straze
        w.println(state.sideOnTurn() == PlayingSide.BLUE ? "BLUE" : "ORANGE");

        state.troopStacks().putToMedia(troopStacksPlainTextMedia);

        if(!state.leaders().isPlaced(PlayingSide.BLUE) && !state.leaders().isPlaced(PlayingSide.ORANGE)) {
            w.println("NL");
        } else {

            if (state.leaders().isPlaced(PlayingSide.BLUE)) {
                w.println("OL " + state.leaders().position(PlayingSide.BLUE));
            } else {
                w.println("OL X " + state.leaders().position(PlayingSide.ORANGE));
            }
        }

        boardPlainTextMediamedia.putBoard(state.board());

        return null;
    }

    @Override
    public Void putPlacingGuardsGameState(PlacingGuardsGameState state) {
        PrintWriter w = writer();

        w.println("GUARDS");
        w.println(state.guardsCount());
        w.println(state.sideOnTurn() == PlayingSide.BLUE ? "BLUE" : "ORANGE");

        state.troopStacks().putToMedia(troopStacksPlainTextMedia);

        String blue, orange;

        if (state.leaders().isPlaced(PlayingSide.BLUE)) {
            blue = state.leaders().position(PlayingSide.BLUE).toString();
        } else {
            blue = "X";
        }

        if (state.leaders().isPlaced(PlayingSide.ORANGE)) {
            orange = state.leaders().position(PlayingSide.ORANGE).toString();
        } else {
            orange = "X";
        }

        w.println("BL " + blue + " " + orange);
        boardPlainTextMediamedia.putBoard(state.board());

        return null;
    }

    @Override
    public Void putMiddleGameState(MiddleGameState state) {
        PrintWriter w = writer();

        w.println("MIDDLE");
        w.println(4); //TODO: pocet strazi
        w.println(state.sideOnTurn() == PlayingSide.BLUE ? "BLUE" : "ORANGE");

        state.troopStacks().putToMedia(troopStacksPlainTextMedia);

        String blue, orange;

        if (state.leaders().isPlaced(PlayingSide.BLUE)) {
            blue = state.leaders().position(PlayingSide.BLUE).toString();
        } else {
            blue = "X";
        }

        if (state.leaders().isPlaced(PlayingSide.ORANGE)) {
            orange = state.leaders().position(PlayingSide.ORANGE).toString();
        } else {
            orange = "X";
        }

        w.println("BL " + blue + " " + orange);
        boardPlainTextMediamedia.putBoard(state.board());

        return null;
    }

    @Override
    public Void putFinishedGameState(VictoryGameState state) {
        PrintWriter w = writer();

        w.println("VICTORY");
        w.println(4); // TODO: pocet strazi
        w.println(state.sideOnTurn() == PlayingSide.BLUE ? "BLUE" : "ORANGE");

        state.troopStacks().putToMedia(troopStacksPlainTextMedia);

        if (state.leaders().isPlaced(PlayingSide.ORANGE)) {
            w.println("OL X " + state.leaders().position(PlayingSide.ORANGE));
        } else {
            w.println("OL " + state.leaders().position(PlayingSide.BLUE) + " X");
        }

        boardPlainTextMediamedia.putBoard(state.board());
        return null;
    }
}
