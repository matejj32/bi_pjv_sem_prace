package wtf.thedrake.media.plaintext;

import wtf.thedrake.game.BothLeadersPlaced;
import wtf.thedrake.game.NoLeadersPlaced;
import wtf.thedrake.game.OneLeaderPlaced;
import wtf.thedrake.media.LeadersMedia;
import wtf.thedrake.media.PrintMedia;

import java.io.OutputStream;

public class LeadersPlainTextMedia extends PrintMedia implements LeadersMedia<Void> {
    public LeadersPlainTextMedia(OutputStream stream) {
        super(stream);
    }

    @Override
    public Void putNoLeadersPlaced(NoLeadersPlaced leaders) {
        //TODO
        return null;
    }

    @Override
    public Void putOneLeaderPlaced(OneLeaderPlaced leaders) {
        //TODO
        return null;
    }

    @Override
    public Void putBothLeadersPlaced(BothLeadersPlaced leaders) {
        //TODO
        return null;
    }
}
