package wtf.thedrake.media.plaintext;

import wtf.thedrake.game.BasicTroopStacks;
import wtf.thedrake.game.PlayingSide;
import wtf.thedrake.game.TroopInfo;
import wtf.thedrake.media.BoardMedia;
import wtf.thedrake.media.PrintMedia;
import wtf.thedrake.media.TroopStacksMedia;

import java.io.OutputStream;
import java.io.PrintWriter;


public class TroopStacksPlainTextMedia extends PrintMedia implements TroopStacksMedia<Void> {
    public TroopStacksPlainTextMedia(OutputStream stream) {
        super(stream);
    }

    @Override
    public Void putBasicTroopStacks(BasicTroopStacks stacks) {
        PrintWriter w = writer();
        w.print("BLUE stack: ");
        for (TroopInfo info :stacks.troops(PlayingSide.BLUE)){
            if(stacks.troops(PlayingSide.BLUE).indexOf(info) == stacks.troops(PlayingSide.BLUE).size()-1){
                w.print(info.name());
            }else {
                w.print(info.name() + " ");
            }
        }
        w.println();
        w.print("ORANGE stack: ");
        for (TroopInfo info :stacks.troops(PlayingSide.ORANGE)){
            if(stacks.troops(PlayingSide.ORANGE).indexOf(info) == stacks.troops(PlayingSide.ORANGE).size()-1){
                w.print(info.name());
            }else {
                w.print(info.name() + " ");
            }
        }
        w.println();

        return null;
    }
}
