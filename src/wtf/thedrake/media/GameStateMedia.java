package wtf.thedrake.media;

import wtf.thedrake.game.MiddleGameState;
import wtf.thedrake.game.PlacingGuardsGameState;
import wtf.thedrake.game.PlacingLeadersGameState;
import wtf.thedrake.game.VictoryGameState;

public interface GameStateMedia<T> {
	public T putPlacingLeadersGameState(PlacingLeadersGameState state);
	public T putPlacingGuardsGameState(PlacingGuardsGameState state);
	public T putMiddleGameState(MiddleGameState state);
	public T putFinishedGameState(VictoryGameState state);
}

