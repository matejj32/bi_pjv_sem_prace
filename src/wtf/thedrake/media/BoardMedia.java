package wtf.thedrake.media;

import wtf.thedrake.game.Board;

public interface BoardMedia<T> {
	public T putBoard(Board board);
}
