package wtf.thedrake.media;

import wtf.thedrake.game.BasicTroopStacks;

public interface TroopStacksMedia<T> {
	public T putBasicTroopStacks(BasicTroopStacks stacks);
}
