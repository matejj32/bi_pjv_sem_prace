package wtf.thedrake.ui;

import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import wtf.thedrake.game.*;

import java.util.List;

/**
 * Class ensure display a board in application window
 */
public class StackView extends GridPane {

    private GameState state;
    private Stage stage;
    private BoardView context;

    /**
     * Constructor
     */
    public StackView() {
        this.setVgap(6);
        this.setHgap(1);
        this.setPadding(new Insets(5));

        for (int y = 0; y < 4; y++) {
            this.add(new Label("lol"), 0, y);
        }
    }

    /**
     * Setter game state
     *
     * @param state
     */
    public void setGameState(GameState state) {
        this.state = state;
    }

    /**
     * Setter stage what is element of GUI in previous scene
     *
     * @param stage
     */
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    /**
     * Update whole board
     */
    public void update() {
        this.getChildren().clear();

        int y = 0;
        for (TroopInfo troopInfo : state.troopStacks().troops(state.sideOnTurn())) {
            this.add(new Label(troopInfo.name()), 0, y);
            if (y == 4) {
                break;
            }
            y++;
        }
    }

    public void setContext(BoardView context) {
        this.context = context;
    }
}
