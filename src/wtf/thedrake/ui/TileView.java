package wtf.thedrake.ui;

import wtf.thedrake.game.Move;
import wtf.thedrake.game.Tile;
import wtf.thedrake.game.TilePosition;
import javafx.event.EventHandler;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;

/**
 * Class ensure display a tile and troop of tile which stand on the tile
 */
public class TileView extends Pane {
	private final TilePosition position;
	private final TileBackgrounds backgrounds;
	private final Border selectBorder = new Border(
			new BorderStroke(
					Color.BLACK, 
					BorderStrokeStyle.SOLID, 
					null, 
					new BorderWidths(2)));
	
	private Tile tile;
	private final GameViewContext context;
	private ImageView moveImage;
	private Move move;

    /**
     * Constructor
     * @param position
     * @param context
     */
	public TileView(TilePosition position, GameViewContext context) {
		this.position = position;
		this.backgrounds = new TileBackgrounds();
		this.context = context;
		this.moveImage = new ImageView(getClass().getResource("../assets/tile/move.png").toString());
		this.moveImage.setVisible(false);
		this.getChildren().add(moveImage);

		this.setPrefSize(100, 100);
		this.setMinSize(USE_PREF_SIZE, USE_PREF_SIZE);
		this.setMaxSize(USE_PREF_SIZE, USE_PREF_SIZE);
		this.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				onClicked(event);
			}			
		});
	}

    /**
     * Getter position
     * @return TilePosition
     */
	public TilePosition position() {
		return position;
	}

    /**
     * Setter tile
     * @param tile
     */
	public void setTile(Tile tile) {
		this.tile = tile;
	}

    /**
     * Update a background tile
     */
	public void update() {
		this.setBackground(backgrounds.get(tile));
	}

    /**
     * Select a tile when player click on tile
     */
	public void select() {
		this.setBorder(selectBorder);
		context.setMoves(position);
	}

    /**
     * Unselect a tile when player click on another tile
     */
	public void unselect() {
		this.setBorder(null);
	}

    /**
     * Move a tile
     * @param move
     */
	public void setMove(Move move) {
		moveImage.setVisible(true);
		this.move = move;
	}

    /**
     * Getter move
     * @return Move
     */
	public Move move() {
		return move;	
	}

    /**
     * Remove a tile from board
     */
	public void clearMove() {
		moveImage.setVisible(false);
		move = null;
	}

    /**
     * Handle action throwed by click from player
     * @param event
     */
	private void onClicked(MouseEvent event) {
		if(this.move != null)
			context.executeMove(move);
		else {
			context.clearSelection();
			if(tile.hasTroop()) { 		
				select();
			}
		}
		
		
	}
}
