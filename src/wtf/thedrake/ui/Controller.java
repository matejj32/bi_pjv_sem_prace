package wtf.thedrake.ui;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import wtf.thedrake.game.GameState;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    @FXML
    public void handleMultiPlayerButtonAction(ActionEvent event) {
        System.out.println("Multi Player");
        switchToGameScene(event);
    }

    @FXML
    public void handleEndButtonAction(ActionEvent event) {
        Platform.exit();
    }

    /**
     * Switch to game scene
     *
     * @param event
     */
    private void switchToGameScene(ActionEvent event) {
        HBox root = new HBox();
        Scene scene = new Scene(root);

        // left
        VBox left = new VBox();
        root.getChildren().add(left);

        // right
        VBox right = new VBox();
        root.getChildren().add(right);

        // captured
        HBox captured = new HBox();
        left.getChildren().add(captured);
        left.setSpacing(5.0);
        captured.setAlignment(Pos.TOP_CENTER);
        Label a = new Label("0");
        a.paddingProperty().setValue(new Insets(0, 10.0, 0, 0));
        captured.getChildren().add(a);
        Label b = new Label("0");
        b.paddingProperty().setValue(new Insets(0, 0, 0, 10.0));
        captured.getChildren().add(b);

        TheDrakeApplication application = new TheDrakeApplication();
        GameState state = application.createTestGame();
//        GameState state = application.createBeginGame(); //TODO: nezapomenout prenastavit !!!

        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();

        // stack
        StackView stackView = new StackView();
        stackView.setGameState(state);
        stackView.setStage(stage);
        right.getChildren().add(stackView);

        // board
        BoardView boardView = new BoardView();
        left.getChildren().add(boardView);

        boardView.setGameState(state);
        boardView.setCaptured(captured);
        boardView.setStack(stackView);
        boardView.update();
        // zajisti prepnuti - prenastaveni sceny na novou
        boardView.setStage(stage);
        stage.setScene(scene);

        stackView.update();
    }

    /**
     * Switch to introduction scene
     *
     * @param stage
     * @throws Exception
     */
    public void switchToIntroScene(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("template.fxml"));

        stage.setTitle("The Drake");
        Scene scene = new Scene(root, 800, 600);
        scene.getStylesheets().add("wtf/thedrake/ui/style.css");
        stage.setMinHeight(600);
        stage.setHeight(600);
        stage.setMinWidth(800);
        stage.setWidth(800);
        stage.setScene(scene);
    }
}
