package wtf.thedrake.ui;

import wtf.thedrake.game.Move;
import wtf.thedrake.game.TilePosition;

public interface GameViewContext {
	public void clearSelection();
	public void setMoves(TilePosition position); 
	public void executeMove(Move move);
}
