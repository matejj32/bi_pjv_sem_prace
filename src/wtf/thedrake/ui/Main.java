package wtf.thedrake.ui;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;
import javafx.util.Duration;
import java.net.URL;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("template.fxml"));

        primaryStage.setTitle("The Drake");
        Scene scene = new Scene(root, 800, 600);
        scene.getStylesheets().add("wtf/thedrake/ui/style.css");
        primaryStage.setScene(scene);
        primaryStage.setMinHeight(600);
        primaryStage.setHeight(600);
        primaryStage.setMinWidth(800);
        primaryStage.setWidth(800);
        primaryStage.show();

        startMusic();

        Platform.setImplicitExit(true);
    }

    private void startMusic() {
        URL resource = getClass().getResource("../assets/music/theme.mp3");
        MediaPlayer a = new MediaPlayer(new Media(resource.toString()));
        a.setOnEndOfMedia(() -> a.seek(Duration.ZERO));
        a.play();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
