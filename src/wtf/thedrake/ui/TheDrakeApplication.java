package wtf.thedrake.ui;

import wtf.thedrake.game.*;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class TheDrakeApplication extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		BoardView boardView = new BoardView();
		Scene scene = new Scene(boardView);
		stage.setScene(scene);
		stage.setTitle("The Drake");
		
		//GameState state = createTestGame();
		GameState state = createBeginGame();
		boardView.setGameState(state);
		boardView.update();
		stage.show();
	}

    /**
     * Vytvori testovaci desku s jiz nahodne rozmistenymi figurkami
     *
     * @return Board
     */
	private Board createTestBoard() {
		StandardDrakeSetup setup = new StandardDrakeSetup();
		Board board = new Board(
				4, 
				new CapturedTroops(),
				new TroopTile(new TilePosition("a1"), new Troop(setup.MONK, PlayingSide.BLUE)),
				new TroopTile(new TilePosition("b1"), new Troop(setup.DRAKE, PlayingSide.BLUE)),
				new TroopTile(new TilePosition("a2"), new Troop(setup.SPEARMAN, PlayingSide.BLUE)),
				new TroopTile(new TilePosition("c2"), new Troop(setup.CLUBMAN, PlayingSide.BLUE)),
				new TroopTile(new TilePosition("a4"), new Troop(setup.ARCHER, PlayingSide.ORANGE, TroopFace.BACK)),
				new TroopTile(new TilePosition("b4"), new Troop(setup.DRAKE, PlayingSide.ORANGE, TroopFace.BACK)),
				new TroopTile(new TilePosition("c3"), new Troop(setup.SWORDSMAN, PlayingSide.ORANGE)));
		return board;
	}

    /**
     * Vytvori testovaci stav hry ve fazi, kdy uz jsou rozmisteni vudci a strazci.
     *
     * @return MiddleGameState
     */
	public GameState createTestGame() {
		Board board = createTestBoard();
		StandardDrakeSetup setup = new StandardDrakeSetup();
		return new MiddleGameState(
				board, 
					new BasicTroopStacks(setup.CLUBMAN), 
					new BothLeadersPlaced(new TilePosition("b1"), new TilePosition("b4")), 
					PlayingSide.BLUE);
	}

    /**
     * Vytvori stav hry od uplneho zacatku, tj. zacina se rozmistenim vudcu obou barev na plochu. Zacina modra.
     *
     * @return PlacingLeadersGameState
     */
	public GameState createBeginGame(){
		Board board = new Board(4, new CapturedTroops());
		StandardDrakeSetup setup = new StandardDrakeSetup();
		return new PlacingLeadersGameState(board, new BasicTroopStacks(setup.DRAKE), new NoLeadersPlaced(),PlayingSide.BLUE);
	}
}
