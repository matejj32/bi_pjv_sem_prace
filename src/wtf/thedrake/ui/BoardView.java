package wtf.thedrake.ui;

import java.util.List;

import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import wtf.thedrake.game.GameState;
import wtf.thedrake.game.Move;
import wtf.thedrake.game.PlayingSide;
import wtf.thedrake.game.TilePosition;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.layout.GridPane;

/**
 * Class ensure display a board in application window
 */
public class BoardView extends GridPane implements GameViewContext {

    private GameState state;
    private Stage stage;
    private HBox captured;
    private StackView stack;

    /**
     * Constructor
     */
    public BoardView() {
        this.setVgap(6);
        this.setHgap(6);
        this.setPadding(new Insets(5));

        for (int y = 0; y < 4; y++) {
            for (int x = 0; x < 4; x++) {
                int i = x;
                int j = 3 - y;
                this.add(new TileView(new TilePosition(i, j), this), x, y);
            }
        }
    }

    /**
     * Return a tile view of tile position by index got from children
     *
     * @param pos
     * @return TileView
     */
    public TileView tileViewAt(TilePosition pos) {
        int index = (3 - pos.j) * 4 + pos.i;
        return (TileView) getChildren().get(index);
    }

    /**
     * Setter game state
     *
     * @param state
     */
    public void setGameState(GameState state) {
        this.state = state;
    }

    /**
     * Setter stage what is element of GUI in previous scene
     *
     * @param stage
     */
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    /**
     * Update whole board
     */
    public void update() {
        for (Node n : this.getChildren()) {
            TileView view = (TileView) n;
            view.unselect();
            view.clearMove();
            view.setTile(state.board().tileAt(view.position()));
            view.update();
        }

        ((Label) captured.getChildren().get(0)).setText("B: " + String.valueOf(state.board().captured().getCapturedBlueTroops().size()));
        ((Label) captured.getChildren().get(1)).setText("O: " + String.valueOf(state.board().captured().getCapturedOrangeTroops().size()));
    }

    /**
     * For all children call unselect to reset checking of tile by player
     */
    @Override
    public void clearSelection() {
        clearAllMoves();
        for (Node n : this.getChildren()) {
            TileView view = (TileView) n;
            view.unselect();
        }
    }

    /**
     * Set possible moves for chosen tile position
     *
     * @param position
     */
    @Override
    public void setMoves(TilePosition position) {
        clearAllMoves();
        List<Move> moves = state.boardMoves(position);
        for (Move move : moves) {
            tileViewAt(move.target()).setMove(move);
        }
    }

    /**
     * Delegate unchecking of all tile on the board
     */
    public void clearAllMoves() {
        for (Node n : this.getChildren()) {
            TileView view = (TileView) n;
            view.clearMove();
        }
    }

    /**
     * Perform the game logic on the view tier
     *
     * @param move
     */
    @Override
    public void executeMove(Move move) {
        GameState newState = move.resultState();
        setGameState(newState);
        update();
        stack.update();
        if (newState.isVictory()) {
            System.out.println((newState.sideOnTurn().opposite() == PlayingSide.BLUE ? "Blue" : "Orange") + " won !");
            showAlertAndSwitchToIntroScene(newState.sideOnTurn());
        }
    }

    /**
     * Display message to player about winning and switch to introduction scene
     *
     * @param sideOnTurn
     */
    private void showAlertAndSwitchToIntroScene(PlayingSide sideOnTurn) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Game over !");
        alert.setHeaderText((sideOnTurn.opposite() == PlayingSide.BLUE ? "Blue" : "Orange") + " won !");

        if (alert.showAndWait().get() == ButtonType.OK) {
            try {
                new Controller().switchToIntroScene(stage);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void setCaptured(HBox captured) {
        this.captured = captured;
    }

    public void setStack(StackView stack) {
        this.stack = stack;
        stack.setContext(this);
    }
}
